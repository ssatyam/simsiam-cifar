import os
import torch
import torch.nn as nn
import torch.nn.functional as F 
import torchvision
import numpy as np
# from tqdm import tqdm
from arguments_vanilla_cl import get_args
from augmentations import get_aug
from models import get_model
from tools import AverageMeter, knn_monitor, Logger, file_exist_check
from datasets import get_dataset
from optimizers import get_optimizer, LR_Scheduler
from linear_eval import main as linear_eval
from datetime import datetime


import torchvision.transforms as T
cifar10_mean_std = [[0.4914, 0.4822, 0.4465], [0.2023, 0.1994, 0.2010]]

class SimSiamTransform_CIFAR10():
    def __init__(self, image_size, mean_std=cifar10_mean_std, jitter_num=0.4, grayscale_prob=0.2):
        image_size = 224 if image_size is None else image_size  # by default simsiam use image size 224
        p_blur = 0.5 if image_size > 32 else 0  # exclude cifar
        # the paper didn't specify this, feel free to change this value
        # I use the setting from simclr which is 50% chance applying the gaussian blur
        # the 32 is prepared for cifar training where they disabled gaussian blur
        print("Using simsiam cifar10 transform...")
        self.transform = T.Compose([
            T.RandomResizedCrop(image_size, scale=(0.2, 1.0)),
            T.RandomHorizontalFlip(),
            T.RandomApply([T.ColorJitter(jitter_num, jitter_num, jitter_num, 0.1)], p=0.8),
            T.RandomGrayscale(p=grayscale_prob),
            T.RandomApply([T.GaussianBlur(kernel_size=image_size // 20 * 2 + 1, sigma=(0.1, 2.0))], p=p_blur),
            T.ToTensor(),
            T.Normalize(*mean_std)
        ])

    def __call__(self, x):
        x1 = self.transform(x)
        x2 = self.transform(x)
        return x1, x2

def main(device, args):

    train_loader = torch.utils.data.DataLoader(
        dataset=get_dataset(
            transform=get_aug(train=True, dataset=args.dataset.name, **args.aug_kwargs),
            train=True,
            **args.dataset_kwargs),
        shuffle=True,
        batch_size=args.train.batch_size,
        **args.dataloader_kwargs
    )
    memory_loader = torch.utils.data.DataLoader(
        dataset=get_dataset(
            transform=get_aug(train=False, train_classifier=False, dataset=args.dataset.name, **args.aug_kwargs),
            train=True,
            **args.dataset_kwargs),
        shuffle=False,
        batch_size=args.train.batch_size,
        **args.dataloader_kwargs
    )
    test_loader = torch.utils.data.DataLoader(
        dataset=get_dataset( 
            transform=get_aug(train=False, train_classifier=False, dataset=args.dataset.name, **args.aug_kwargs),
            train=False,
            **args.dataset_kwargs),
        shuffle=False,
        batch_size=args.train.batch_size,
        **args.dataloader_kwargs
    )

    # define model
    model = get_model(args.model).to(device)


    # define optimizer
    optimizer = get_optimizer(
        args.train.optimizer.name, model, 
        lr=args.train.base_lr*args.train.batch_size/256, 
        momentum=args.train.optimizer.momentum,
        weight_decay=args.train.optimizer.weight_decay)

    start_epoch = 0

    lr_scheduler = LR_Scheduler(
        optimizer,
        args.train.warmup_epochs, args.train.warmup_lr*args.train.batch_size/256, 
        args.train.num_epochs, args.train.base_lr*args.train.batch_size/256, args.train.final_lr*args.train.batch_size/256, 
        len(train_loader),
        constant_predictor_lr=True # see the end of section 4.2 predictor
    )
    # check if resume
    if args.resume:
        checkpoint = torch.load(args.resume_from)
        model.load_state_dict(checkpoint['state_dict'])
        optimizer.load_state_dict(checkpoint['optimizer_state_dict'])
        lr_scheduler.load_state_dict(checkpoint["lr_scheduler_state_dict"], optimizer)
        start_epoch = checkpoint['epoch'] + 1
        accuracy = checkpoint['accuracy']
        print(f"Resuming from {args.resume_from} \n"
              f"at epoch {start_epoch - 1} \n"
              f"with accuracy {accuracy} \n"
              f"lr at: {lr_scheduler.current_lr}")

    model = torch.nn.DataParallel(model)

    logger = Logger(tensorboard=args.logger.tensorboard, matplotlib=args.logger.matplotlib, log_dir=args.log_dir)
    accuracy = 0
    best_acc = 0
    # Start training
    # global_progress = tqdm(range(0, args.train.stop_at_epoch), desc=f'Training')

    jitter_num = args.color_jitter_max
    grayscale_prob = args.gray_scale_max
    for epoch in range(start_epoch, args.train.stop_at_epoch):
        model.train()

        if args.curriculum:
            if args.c_scheduler == "stepwise":
                if epoch % (args.train.num_epochs // args.cl_step) == 0:
                    jitter_num = (args.color_jitter_max / args.cl_step) * (epoch // (args.train.num_epochs // args.cl_step) + 1)
                    grayscale_prob = (args.gray_scale_max / args.cl_step) * (epoch // (args.train.num_epochs // args.cl_step) + 1)
            else:
                # original color jitter and random gray scale value
                jitter_num = 0.4
                grayscale_prob = 0.2

            train_loader = torch.utils.data.DataLoader(
                dataset=get_dataset(
                    transform=SimSiamTransform_CIFAR10(image_size=args.dataset.image_size, jitter_num=jitter_num, grayscale_prob=grayscale_prob),
                    train=True,
                    **args.dataset_kwargs),
                shuffle=True,
                batch_size=args.train.batch_size,
                **args.dataloader_kwargs
            )

            print(f"Epoch: {epoch}, jitter_num: {jitter_num}, grayscale_prob: {grayscale_prob}")


        for idx, ((images1, images2), labels) in enumerate(train_loader):

            model.zero_grad()
            data_dict = model.forward(images1.to(device, non_blocking=True), images2.to(device, non_blocking=True))
            loss = data_dict['loss'].mean() # ddp
            loss.backward()
            optimizer.step()
            lr_scheduler.step()
            data_dict.update({'lr':lr_scheduler.get_lr()})

            logger.update_scalers(data_dict)

        if args.train.knn_monitor and epoch % args.train.knn_interval == 0: 
            accuracy = knn_monitor(model.module.backbone, memory_loader, test_loader, device, k=min(args.train.knn_k, len(memory_loader.dataset)), hide_progress=args.hide_progress) 
        
        epoch_dict = {"epoch":epoch, "accuracy":accuracy}
        print("Epoch: {}, Accuracy: {}".format(epoch, accuracy))
        logger.update_scalers(epoch_dict)

        if epoch % args.save_interval == 0:
            # Save checkpoint
            model_path = os.path.join(args.ckpt_dir, f"{args.name}_{epoch}.pth")
            torch.save({
                'epoch': epoch,
                'accuracy': accuracy,
                'state_dict': model.module.state_dict(),
                'optimizer_state_dict': optimizer.state_dict(),
                'lr_scheduler_state_dict': lr_scheduler.state_dict()
            }, model_path)
            print(f"Model saved to {model_path}")
            with open(os.path.join(args.log_dir, f"checkpoint_path.txt"), 'w+') as f:
                f.write(f'{model_path}')

        if best_acc < accuracy:
            best_acc = accuracy
            # Save best checkpoint
            best_model_path = os.path.join(args.ckpt_dir, f"model_best.pth")
            torch.save({
                'epoch': epoch,
                'accuracy': accuracy,
                'state_dict': model.module.state_dict(),
                'optimizer_state_dict': optimizer.state_dict(),
                'lr_scheduler_state_dict': lr_scheduler.state_dict()
            }, best_model_path)
            print(f"Best model saved")
            with open(os.path.join(args.log_dir, f"checkpoint_path.txt"), 'w+') as f:
                f.write(f'{best_model_path}')

    # Save checkpoint
    model_path = os.path.join(args.ckpt_dir, f"{args.name}_final_epoch.pth")
    torch.save({
        'epoch': epoch + 1,
        'accuracy': accuracy,
        'state_dict': model.module.state_dict(),
        'optimizer_state_dict': optimizer.state_dict(),
        'lr_scheduler_state_dict': lr_scheduler.state_dict()
    }, model_path)
    print(f"Model saved to {model_path}")
    with open(os.path.join(args.log_dir, f"checkpoint_path.txt"), 'w+') as f:
        f.write(f'{model_path}')

    if args.eval is not False:
        args.eval_from = model_path
        linear_eval(args)


if __name__ == "__main__":
    args = get_args()

    main(device=args.device, args=args)

    completed_log_dir = args.log_dir.replace('in-progress', 'debug' if args.debug else 'completed')



    os.rename(args.log_dir, completed_log_dir)
    print(f'Log file has been saved to {completed_log_dir}')















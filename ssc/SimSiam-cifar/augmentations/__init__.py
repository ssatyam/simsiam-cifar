from .simsiam_aug import SimSiamTransform_CIFAR100, SimSiamTransform_CIFAR10, SimSiamTransform_STL10
from .eval_aug import Transform_single
from .byol_aug import BYOL_transform_CIFAR100, BYOL_transform_IMAGENET, BYOL_transform_CIFAR10
from .simclr_aug import SimCLRTransform


def get_aug(name='simsiam', image_size=224, train=True, train_classifier=None, dataset="cifar10"):
    if dataset == "cifar10":
        if train == True:
            if name == 'simsiam' or name == 'simsiam_mixup':
                augmentation = SimSiamTransform_CIFAR10(image_size)
            elif name == 'byol' or name == 'byol_mixup':
                augmentation = BYOL_transform_CIFAR10(image_size)
            elif name == 'simclr' or name == 'simclr_mixup':
                augmentation = SimCLRTransform(image_size)
            else:
                raise NotImplementedError
        elif train == False:
            if train_classifier is None:
                raise Exception
            augmentation = Transform_single(image_size, train=train_classifier)
        else:
            raise Exception
    elif dataset == "cifar100":
        if train == True:
            if name == 'simsiam' or name == 'simsiam_mixup':
                augmentation = SimSiamTransform_CIFAR100(image_size)
            elif name == 'byol' or name == 'byol_mixup':
                augmentation = BYOL_transform_CIFAR100(image_size)
            elif name == 'simclr' or name == 'simclr_mixup':
                augmentation = SimCLRTransform(image_size)
            else:
                raise NotImplementedError
        elif train == False:
            if train_classifier is None:
                raise Exception
            augmentation = Transform_single(image_size, train=train_classifier)
        else:
            raise Exception
    elif dataset == "stl10":
        if train == True:
            if name == 'simsiam' or name == 'simsiam_mixup':
                augmentation = SimSiamTransform_STL10(image_size)
            else:
                raise NotImplementedError
        elif train == False:
            if train_classifier is None:
                raise Exception
            augmentation = Transform_single(image_size, train=train_classifier)
        else:
            raise Exception

    return augmentation









pip install tensorboardx tqdm Pillow matplotlib pyyaml
CUDA_VISIBLE_DEVICES=0 python ../../../main.py \
    --data_dir ~/data \
    --log_dir  ../../../logs/NoMixUp_randcrop_simsiam_cifar10_800_rerun3 \
    -c  ../../../configs/simsiam_cifar10.yaml \
    --ckpt_dir ../../../ckpt/simsiam/cifar10/NoMixUp_randcrop_simsiam_cifar10_800_rerun3 \
    > ../../../train_logs/simsiam/cifar10/NoMixUp_randcrop_simsiam_cifar10_800_rerun3.txt 2>&1
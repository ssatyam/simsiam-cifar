pip install tensorboardx tqdm Pillow matplotlib pyyaml
CUDA_VISIBLE_DEVICES=0 python ../../../main_mixup.py \
    --mixup_lam 0.2 \
    --data_dir ~/data \
    --log_dir ../../../logs/MixUp_alpha0.2_randomcrop_simsiam_cifar10_800_rerun2 \
    -c ../../../configs/simsiam_cifar10_mixup.yaml \
    --ckpt_dir ../../../ckpt/simsiam/cifar10/MixUp_alpha0.2_randomcrop_simsiam_cifar10_800_rerun2 \
    > ../../../train_logs/simsiam/cifar10/MixUp_alpha0.2_randomcrop_simsiam_cifar10_800_rerun2.txt 2>&1
CUDA_VISIBLE_DEVICES=0 python ../../../main.py \
    --data_dir ~/data \
    --log_dir  ../../../logs/NoMixUp_randcrop_simsiam_cifar10_800 \
    -c  ../../../configs/simsiam_cifar10.yaml \
    --ckpt_dir ../../../ckpt/simsiam/cifar10/NoMixUp_randcrop_simsiam_cifar10_800 \
    > ../../../train_logs/simsiam/cifar10/NoMixUp_randcrop_simsiam_cifar10_800.txt 2>&1
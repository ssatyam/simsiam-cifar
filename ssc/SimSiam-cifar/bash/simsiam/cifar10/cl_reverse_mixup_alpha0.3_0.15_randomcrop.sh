pip install tensorboardx tqdm Pillow matplotlib pyyaml
CUDA_VISIBLE_DEVICES=0 python ../../../main_mixup.py \
    --mixup_lam 0.2 \
    --curriculum \
    --reverse-c \
    --c-scheduler stepwise \
    --cl_step 4 \
    --lam_max 0.3 \
    --lam_min 0.15 \
    --mixup_option mixup \
    --data_dir ~/data \
    --log_dir ../../../logs/reverse_MixUp_randomcrop_simsiam_curriculum_stepwise_4_alpha-max_0.3_alpha-min_0.15_cifar10_800 \
    -c ../../../configs/simsiam_cifar10_mixup.yaml \
    --ckpt_dir ../../../ckpt/simsiam/cifar10/reverse_MixUp_randomcrop_simsiam_curriculum_stepwise_4_alpha-max_0.3_alpha-min_0.15_cifar10_800 \
    > ../../../train_logs/simsiam/cifar10/reverse_MixUp_randomcrop_simsiam_curriculum_stepwise_4_alpha-max_0.3_alpha-min_0.15_cifar10_800.txt 2>&1
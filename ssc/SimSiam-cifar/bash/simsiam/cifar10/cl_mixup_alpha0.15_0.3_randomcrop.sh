CUDA_VISIBLE_DEVICES=1 python ~/work/simsiam/main_mixup.py \
    --mixup_lam 0.2 \
    --curriculum \
    --c-scheduler stepwise \
    --cl_step 4 \
    --lam_max 0.3 \
    --lam_min 0.15 \
    --mixup_option mixup \
    --data_dir ~/data \
    --log_dir ~/work/simsiam/logs/MixUp_randomcrop_simsiam_curriculum_stepwise_4_alpha-max_0.3_alpha-min_0.15_cifar10_800 \
    -c ~/work/simsiam/configs/simsiam_cifar10_mixup.yaml \
    --ckpt_dir ~/work/simsiam/ckpt/simsiam/cifar10/MixUp_randomcrop_simsiam_curriculum_stepwise_4_alpha-max_0.3_alpha-min_0.15_cifar10_800 \
    > ~/work/simsiam/train_logs/simsiam/cifar10/MixUp_randomcrop_simsiam_curriculum_stepwise_4_alpha-max_0.3_alpha-min_0.15_cifar10_800.txt 2>&1 &
import os
import torch
import torch.nn as nn
import torch.nn.functional as F 
import torchvision
import numpy as np
# from tqdm import tqdm
from arguments_mixup import get_args
from augmentations import get_aug
from models import get_model
from tools import AverageMeter, knn_monitor, Logger, file_exist_check
from datasets import get_dataset
from optimizers import get_optimizer, LR_Scheduler
from linear_eval import main as linear_eval
from datetime import datetime

import torchvision.transforms as T
from PIL import Image
import PIL

class SimSiamTransform_CIFAR10():
    def __init__(self, image_size):
        image_size = 224 if image_size is None else image_size  # by default simsiam use image size 224
        p_blur = 0.5 if image_size > 32 else 0  # exclude cifar
        # the paper didn't specify this, feel free to change this value
        # I use the setting from simclr which is 50% chance applying the gaussian blur
        # the 32 is prepared for cifar training where they disabled gaussian blur
        print("Using simsiam cifar10 transform...")
        self.transform = T.Compose([
            # T.RandomResizedCrop(image_size, scale=(0.2, 1.0)),
            T.RandomHorizontalFlip(),
            T.RandomApply([T.ColorJitter(0.4, 0.4, 0.4, 0.1)], p=0.8),
            T.RandomGrayscale(p=0.2),
            T.RandomApply([T.GaussianBlur(kernel_size=image_size // 20 * 2 + 1, sigma=(0.1, 2.0))], p=p_blur),
            T.ToTensor(),
        ])

    def __call__(self, x):
        x1 = self.transform(x)
        x2 = self.transform(x)
        return x1, x2

def main(device, args):

    train_loader = torch.utils.data.DataLoader(
        dataset=get_dataset(
            transform=SimSiamTransform_CIFAR10(128),
            train=True,
            **args.dataset_kwargs),
        shuffle=True,
        batch_size=1,
        **args.dataloader_kwargs
    )

    transform = T.ToPILImage()

    # Start training
    for epoch in range(0, 4):
        mixup_lambda = [0.3, 0.25, 0.2, 0.15]
        print(f"mixup_lambda: {mixup_lambda[epoch]}")
        for idx, ((images1, images2), labels) in enumerate(train_loader):
            if idx == 0:
                mixed_x2 = mixup_lambda[epoch] * images1 + (1 - mixup_lambda[epoch]) * images2

                images1 = torch.squeeze(images1)
                images2 = torch.squeeze(images2)
                mixed_x2 = torch.squeeze(mixed_x2)

                img_x1 = transform(images1)
                img_x2 = transform(images2)
                img_mixed_x2 = transform(mixed_x2)
                img_x1.save(f"vis/epoch_{epoch}_x1.jpg")
                img_x2.save(f"vis/epoch_{epoch}_x2.jpg")
                img_mixed_x2.save(f"vis/epoch_{epoch}_mixed_x2.jpg")

                break


if __name__ == "__main__":
    args = get_args()

    main(device=args.device, args=args)

    completed_log_dir = args.log_dir.replace('in-progress', 'debug' if args.debug else 'completed')



    os.rename(args.log_dir, completed_log_dir)
    print(f'Log file has been saved to {completed_log_dir}')














